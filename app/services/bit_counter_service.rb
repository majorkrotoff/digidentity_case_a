class BitCounterService

  def count_bits(filename)
    # start with file
    if File.exist?(filename)
      counts = calculate_bits(filename)
      p counts
      puts "found #{counts['cnt_1']} bits set to 1"
      puts "found #{counts['cnt_0']} bits set to 0"
    else
      'Sorry, file doesn\'t exist'
    end
  end


  private

  def prepare_bits_array
    bits_array = []
    i = 0
    while i<=255
      bits_array[i] = i.to_s(2).count('1')
      i+=1
    end
    bits_array
  end

  def calculate_bits(filename)
    # pre-calculations
    bits_array = prepare_bits_array

    cnt_0 = 0
    cnt_1 = 0

    File.open(filename).each_byte do |ch|
      cnt = bits_array[ch]
      cnt_1 += cnt
      cnt_0 += 8 - cnt
    end

    {"cnt_0" => cnt_0, "cnt_1" => cnt_1}
  end

end
