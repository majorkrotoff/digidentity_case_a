# Case A: Bit Counter

To count the bits please do the following:

1. Run the command line Ruby interpreter:

```
#!shell
rails c
```
      
2. Create the service:

```
#!shell
myserv = BitCounterService.new
```
      
3. Run the calculations:

```
#!shell
myserv.count_bits '/path/to/the/picture/file/picture.jpg'
```